import React, { Component } from 'react';
import ymaps from 'ymaps';

import WaypointsList from './waypointsList/waypointsList';
import Map from './map/map';
import AppLoader from './common/appLoader/appLoader';
import YmapsError from './errors/ymapsError/ymapsError';
import './app.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			loadingError: false,
			hideSidebar: true,
			sidebarClass: 'sidebar sidebar-hidden',
			hideButtonClass: 'navigate_next'
		}
		this.handleHideSidebar = this.handleHideSidebar.bind(this);
	}

	componentDidMount() {
		ymaps.load(`https://api-maps.yandex.ru/2.1/?lang=ru_RU&
			load=Map,GeoObjectCollection,GeoObject,Placemark,Polyline,
			geometry.LineString,geocode,geoObject.addon.balloon,
			control.GeolocationControl`).then(() => {
				this.setState({
					loading: false
				});
		})
		.catch((error) => {
			this.setState({
				loading: false,
				loadingError: true
			});
		});
	}

	handleHideSidebar() {
		this.setState((prevState) => ({
			hideSidebar: !prevState.hideSidebar,
			sidebarClass: prevState.hideSidebar ? 'sidebar' : 'sidebar sidebar-hidden',
			hideButtonClass: prevState.hideSidebar ? 'navigate_before' : 'navigate_next'
		}))
	}

	render() {
		if (this.state.loading) {
			return (
				<AppLoader />
			);
		} else if (!this.state.loading && this.state.loadingError) {
			return (
				<YmapsError />
			)
		} else {
			return (
				<div className="app">
					<div className={ this.state.sidebarClass }>
						<div className="app-logo">
							<div className="app-logo-background" />
							<i className="material-icons app-logo-image">directions</i>
							<div className="app-logo-title">Твой путь</div>
						</div>

						<WaypointsList className="main-block-waypoints" />

						<div className="hide-sidebar-button" onClick={ this.handleHideSidebar }>
							<i className="material-icons">
								{ this.state.hideButtonClass }
							</i>
						</div>
					</div>

					<Map className="main-block-map" />

				</div>
			)
		}
	}
}

export default App;
