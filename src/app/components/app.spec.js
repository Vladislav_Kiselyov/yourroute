import React from 'react';
import { shallow } from 'enzyme';

import App from './app';


describe('Тест компонента App',()=>{
    let wrapper;

    beforeEach(()=>{
        wrapper = shallow(<App />);
    });

    it('Приложение запустилось без ошибок', () => {
        expect(wrapper.length).toEqual(1);
    });
    
});