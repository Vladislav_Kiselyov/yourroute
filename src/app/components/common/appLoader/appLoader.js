import React, { Component } from 'react';

import './appLoader.css';
import preloader from '../../../../assets/preloader.svg';


class AppLoader extends Component {

	render() {
		return (
			<div className="align-children-center app-loader-container">
				<div>
					<div className="align-children-center-horizontal">
						<img className="app-loader-icon" src={ preloader } alt="..." />
						{/* <i class="material-icons app-loader-icon">sentiment_dissatisfied</i> */}
					</div>
					<div className="app-loader-title">Идет загрузка</div>
				</div>
			</div>
		);
	}
}

export default AppLoader;
