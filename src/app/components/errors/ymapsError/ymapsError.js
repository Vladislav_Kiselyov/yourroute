import React, { Component } from 'react';
import './ymapsError.css';


class YmapsError extends Component {

	render() {
		return (
			<div className="align-children-center ymaps-error-container">
				<div>
					<div className="align-children-center-horizontal">
						<i className="material-icons ymaps-error-icon">sentiment_dissatisfied</i>
					</div>
					<div className="ymaps-error-title">Не удалось загрузить "Яндекс карты"</div>
				</div>
			</div>
		);
	}
}

export default YmapsError;
