import React, { Component } from 'react';
import { connect } from 'react-redux';

import { store } from '../../store/store';
import './map.css';


class Map extends Component {
	constructor(props) {
		super(props);
		this.state = {
			ymap: null
		}
		store.subscribe(this.updateMap.bind(this));
	}
	componentWillMount() {
		window.ymaps.ready(() => {
			this.setState({
				ymap: new window.ymaps.Map("map", {
					center: [55.76, 37.64],
					controls: ['geolocationControl'],
					zoom: 12
				}, {
					autoFitToViewport: 'always'
				})
			});
		});
	}

	updateMap() {
		if (this.props.mapObjects.points && this.props.mapObjects.line) {
			this.setState((prevState) => {
				let map = prevState.ymap;
	
				map.geoObjects.add(this.props.mapObjects.points);
				map.geoObjects.add(this.props.mapObjects.line);
	
				if (this.props.mapObjects.center) {
					map.setCenter(this.props.mapObjects.center);
				}
	
				return {
					ymap: map
				}
			})
		}
	}

	render() {
		return (
			<div id="map" className="map-container">

			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		mapObjects: state.mapObjects
	}
}

export default connect(mapStateToProps)(Map);
