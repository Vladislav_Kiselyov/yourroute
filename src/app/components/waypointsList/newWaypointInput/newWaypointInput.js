import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { store } from '../../../store/store';
import { addWaypoint } from '../../../store/actions/addWaypoint';
import { searchForWaypoint } from '../../../store/actions/searchForWaypoint';
import './newWaypointInput.css';


class NewWaypointInput extends Component {
	constructor(props) {
		super(props);
		this.state = {
			newWaypointName: '',
			waypointSearchResults: [],
			inputError: null
		};

		store.subscribe(this.getSearchResults.bind(this));
		this.handlePointNameChange = this.handlePointNameChange.bind(this);
		this.handlePointNameKeyPress = this.handlePointNameKeyPress.bind(this);
		this.handlePointNameButtonPress = this.handlePointNameButtonPress.bind(this);
		this.handleSearchItemSelect = this.handleSearchItemSelect.bind(this);
		this.addWaypoint = this.addWaypoint.bind(this);
	}
	
	handlePointNameChange(event) {
		this.setState({
			newWaypointName: event.target.value,
			inputError: null
		});
		this.props.searchForWaypoint(event.target.value);
	}

	handlePointNameKeyPress(event) {
		if (event.charCode === 13) {
			this.addWaypoint(this.props.waypointSearchResults[0]);
		}
	}

	handlePointNameButtonPress() {
		this.addWaypoint(this.props.waypointSearchResults[0]);
	}

	getSearchResults() {
		this.setState({ 
			waypointSearchResults: this.props.waypointSearchResults.map((item, index) => {
				return (
					<div key={ index } index={ index } className="waypoints-search-item" onClick={ this.handleSearchItemSelect }>
						{ item.title }
					</div>
				); 
			})
		});
	}

	handleSearchItemSelect(event) {
		this.addWaypoint(this.props.waypointSearchResults[event.target.attributes.index.value]);
	}

	addWaypoint(waypoint) {
		if (this.state.newWaypointName && waypoint) {
			this.props.addWaypoint(waypoint);
			this.setState({ newWaypointName: '', waypointSearchResults: [] });
			this.props.searchForWaypoint('');
		} else if (!this.state.newWaypointName) {
			this.setState({
				inputError: (
					<div className="new-waypoint-input-error">Введите название точки</div>
				)
			})
		}  else if (!waypoint) {
			this.setState({
				inputError: (
					<div className="new-waypoint-input-error">Точка не найдена</div>
				)
			})
		}
	}

	render() {
		return (
			<div>
				<div className="align-children-center-vertical new-waypoint-input-container">
					<input className="new-waypoint-input" placeholder="Введите адрес" value={ this.state.newWaypointName }
						onChange={ this.handlePointNameChange } onKeyPress={ this.handlePointNameKeyPress }></input>
					<button className="button align-children-center-vertical" onClick={ this.handlePointNameButtonPress }>
						<i className="material-icons">add_box</i>
					</button>

					<div className="waypoints-search-container">
						{ this.state.waypointSearchResults }
					</div>
				</div>

				{ this.state.inputError }
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		waypointSearchResults: state.waypointSearchResults
	};
}

function matchDispatchToProps(dispatch) {
	return bindActionCreators({
		searchForWaypoint: searchForWaypoint,
		addWaypoint: addWaypoint
	}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(NewWaypointInput);
