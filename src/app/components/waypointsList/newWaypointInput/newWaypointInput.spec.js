import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import NewWaypointInput from './newWaypointInput';


describe('Тест компонента NewWaypointInput',()=>{
	let store, wrapper;
	const mockStore = configureStore();

	beforeEach(()=>{
		store = mockStore({});
		wrapper = mount(<NewWaypointInput store={ store } />);
	});

	it('Поле ввода и кнопка присутствуют, поле ввода пустое', () => {
		expect(wrapper.find('input').length).toEqual(1);
		expect(wrapper.find('input').value).toBeUndefined();
		expect(wrapper.find('button').length).toEqual(1);
	});
});