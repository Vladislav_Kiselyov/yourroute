import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import { store } from '../../store/store';
import { moveWaypoint } from '../../store/actions/moveWaypoint';
import NewWaypointInput from './newWaypointInput/newWaypointInput';
import WaypointsListWaypoint from './waypointsListWaypoint/waypointsListWaypoint.js';
import './waypointsList.css';


class WaypointsList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			waypointsList: null
		};

		store.subscribe(this.updateWaypointsList.bind(this));
		this.handleWaypointDrag = this.handleWaypointDrag.bind(this);
	}

	updateWaypointsList() {
		if (this.props.waypoints) {
			this.setState({ 
				waypointsList: this.props.waypoints.map((item, index) => {
					return (
						<Draggable key={ index } draggableId={index} index={index}>
							{(provided, snapshot) => (
								<div ref={provided.innerRef} 
								{...provided.draggableProps}
								{...provided.dragHandleProps}>
									<WaypointsListWaypoint item={ {
										index: index,
										title: item.title
									} } />
								</div>
							)}
						</Draggable>
					); 
				})
			});
		}
	}

	handleWaypointDrag(result) {
		this.props.moveWaypoint(result.source.index, result.destination.index);
	}

	render() {
		return (
			<DragDropContext onDragEnd={ this.handleWaypointDrag }>
				<div className="waypoints-list-container">
					<NewWaypointInput />
							
					<Droppable droppableId="waypoints">
						{(provided, snapshot) => (
							<div ref={provided.innerRef} className="waypoint-list-waypoints">
								{ this.state.waypointsList }
							</div>
						)}
					</Droppable>
				</div>
			</DragDropContext>
		);
	}
}

function mapStateToProps(state) {
	return {
		waypoints: state.waypoints
	}
}

function matchDispatchToProps(dispatch) {
	return bindActionCreators({ moveWaypoint: moveWaypoint }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(WaypointsList);
