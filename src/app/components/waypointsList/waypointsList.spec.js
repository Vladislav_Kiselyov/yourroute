import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import WaypointsList from './waypointsList';

describe('Тест компонента WaypointsList',()=>{
	let store, wrapper;
	const mockStore = configureStore();

	beforeEach(()=>{
		store = mockStore({});
		wrapper = mount(<Provider store={ store } ><WaypointsList /></Provider>);
	});

	it('Изначально список точек маршрута пустой', () => {
		expect(wrapper.find('.waypoints-list-container').length).toEqual(1);
		expect(wrapper.find('.waypoint-list-waypoints').children().length).toEqual(0);
	});
	
});