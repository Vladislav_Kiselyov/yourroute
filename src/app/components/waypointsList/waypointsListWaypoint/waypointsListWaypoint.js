import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { deleteWaypoint } from '../../../store/actions/deleteWaypoint';
import './waypointsListWaypoint.css';

class WaypointsListWaypoint extends Component {
	constructor(props) {
		super();
		this.handleDeleteItem = this.handleDeleteItem.bind(this);
	}

	handleDeleteItem() {
		this.props.deleteWaypoint(this.props.item.index);
	};

	render() {
		return (
			<div className="waypoints-list-waypoint align-children-center-vertical">
				<span className="waypoints-list-waypoint-name">
					{ this.props.item.title }
				</span>
				<button className="button waypoints-list-waypoint-button" onClick={ this.handleDeleteItem } >
					<i className="material-icons">delete</i>
				</button>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {};
}

function matchDispatchToProps(dispatch) {
	return bindActionCreators({ deleteWaypoint: deleteWaypoint }, dispatch);
}

export default  connect(mapStateToProps, matchDispatchToProps)(WaypointsListWaypoint);
