import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import WaypointsListWaypoint from './waypointsListWaypoint';


describe('Тест компонента WaypointsListWaypoint',()=>{
	let store, wrapper, instance;
	const mockStore = configureStore();

	beforeEach(()=>{
		store = mockStore({});
		wrapper = mount(<WaypointsListWaypoint store={ store } item={ {
			index: 0,
			title: 'Новая точка'
		} } />);	
	});

	it('Рендерится и отображает название точки', () => {
		expect(wrapper.find('.waypoints-list-waypoint-name').text()).toEqual('Новая точка');
	});
	
});