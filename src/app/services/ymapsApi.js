class YmapsApiService {
    findPlace(query) {
        return window.ymaps.geocode(query);
    }
}

export default new YmapsApiService();