import YmapsApiService from './ymapsApi';

describe('Тест сервиса YmapsApiService', () => {
	it('Функция findPlace() возвращает правильный результат', () => {
		expect.assertions(1);
		return YmapsApiService.findPlace('Новая точка')
		.then((result) => {
			expect(result).toEqual([0, 0]);
		});
    });
});