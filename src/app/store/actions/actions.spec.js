import { addWaypoint } from './addWaypoint';
import { deleteWaypoint } from './deleteWaypoint';
import { getWaypointSearchResult } from './getWaypointSearchResult';
import { moveWaypoint } from './moveWaypoint';


describe('Тест action creators', () => {
    it('addWaypoint возвращает экшн корректно', () => {
		const newPoint = { title: 'Новая точка', coordinates: [0, 0] };
		expect(addWaypoint(newPoint)).toEqual({ type: 'ADD_NEW_WAYPOINT', payload: newPoint });
	});
	
    it('deleteWaypoint возвращает экшн корректно', () => {
		expect(deleteWaypoint(0)).toEqual({ type: 'DELETE_WAYPOINT', payload: 0 });
	});
	
    it('getWaypointSearchResult возвращает экшн корректно', () => {
		const searchResult = [{ title: 'Новая точка', coordinates: [0, 0] }];
		expect(getWaypointSearchResult(searchResult)).toEqual({
            type: 'GET_WAYPOINT_SEARCH_RESULT', payload: searchResult
        });
	});
	
    it('moveWaypoint возвращает экшн корректно', () => {
		expect(moveWaypoint(1, 0)).toEqual({ type: 'MOVE_WAYPOINT', payload: { 
            previousIndex: 1,
            newIndex: 0
        }});
    });
});