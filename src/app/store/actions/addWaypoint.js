export const addWaypoint = (waypoint) => {
    return {
        type: 'ADD_NEW_WAYPOINT',
        payload: waypoint
    }
};