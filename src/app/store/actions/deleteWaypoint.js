export const deleteWaypoint = (index) => {
    return {
        type: 'DELETE_WAYPOINT',
        payload: index
    }
};