export const getWaypointSearchResult = (result) => {
    return {
        type: 'GET_WAYPOINT_SEARCH_RESULT',
        payload: result
    }
};