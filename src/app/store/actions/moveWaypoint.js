export const moveWaypoint = (previousIndex, newIndex) => {
    return {
        type: 'MOVE_WAYPOINT',
        payload: { 
            previousIndex: previousIndex,
            newIndex: newIndex
        }
    }
};