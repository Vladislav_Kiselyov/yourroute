import YmapsApiService from '../../services/ymapsApi';
import { getWaypointSearchResult } from './getWaypointSearchResult';

export const searchForWaypoint = (query) => {
    return (dispatch) => {
		YmapsApiService.findPlace(query).then(
			function(res) {
                let result = [];

                res.geoObjects.each((item) => {
                    if (item.getAddressLine()) {
                        result.push({
                            title: item.getAddressLine(),
                            coordinates: item.geometry.getCoordinates()
                        })
                    }
                })
                
                dispatch(getWaypointSearchResult(result));
			},
			function(err) {
                console.log('Error', err)
				// обработка ошибки
			}
        );
    };
};