import 'jest';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { YmapsApiService } from '../../services/ymapsApi';
import { searchForWaypoint } from './searchForWaypoint';


jest.mock('../../services/ymapsApi', () => {
	return {
		findPlace: function(query) {
			return new Promise((resolve, reject) => {
				resolve({
					geoObjects: {
						each: function(callback) {
							callback({
								getAddressLine: function() {
									return 'Новая точка'
								},
								geometry: {
									getCoordinates: function() {
										return [0, 0];
									}
								}
							});
						}
					}
				});
			});
		}
	}
})

describe('Тест санка searchForWaypoint', () => {
	let store;
	const mockStore = configureMockStore([ thunk ]);
	
	beforeEach(() => {
		store = mockStore({});
	});

	it('Возвращает правильный экшн', (done) => {
		searchForWaypoint('Новая точка')(store.dispatch)
		setTimeout(() => {
			let action = store.getActions()[0];
			expect(action.type).toEqual('GET_WAYPOINT_SEARCH_RESULT');
			expect(action.payload[0]).toEqual({
				title: 'Новая точка',
				coordinates: [0, 0]
			});
			done();
		}, 50)
    });
});