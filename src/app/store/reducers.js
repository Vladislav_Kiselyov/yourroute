import { combineReducers } from 'redux';
import WaypointsReducer from './reducers/waypoints';
import MapReducer from './reducers/map';
import NewWaypointReducer from './reducers/newWaypointInput';

const Reducers = combineReducers({
    waypoints: WaypointsReducer,
    mapObjects: MapReducer,
    waypointSearchResults: NewWaypointReducer
})

export default Reducers;