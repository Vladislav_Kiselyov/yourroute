const initialState = {
    points: null,
    line: null,
    center: []
}

export default function mapReducer(state = initialState, action) {
    switch (action.type) {
        case 'ADD_NEW_WAYPOINT':
            let newPoint = new window.ymaps.Placemark(action.payload.coordinates, {
                balloonContent: action.payload.title
            }, {
                draggable: true
            });

            if (!state.points) {
                state.points = new window.ymaps.GeoObjectCollection();
                state.line = new window.ymaps.Polyline(new window.ymaps.geometry.LineString());
            }

            newPoint.events.add('drag', function (event) {
                let target = event.get('target');
                state.line.geometry.set(state.points.indexOf(target), target.geometry.getCoordinates());
            });
            
            state.points.add(newPoint);
            state.line.geometry.set(state.line.geometry.getLength(), action.payload.coordinates);
            state.center = action.payload.coordinates;

            return state;
        case 'DELETE_WAYPOINT':
            state.points.splice(action.payload, 1);
            state.line.geometry.remove(action.payload);
            if (state.points.getLength() > 0) {
                state.center = state.points.get(state.points.getLength() -1).geometry.getCoordinates();
            }
            
            return state;
        case 'MOVE_WAYPOINT':
            let item = state.points.splice(action.payload.previousIndex, 1).get(0);
            let coordinates = state.line.geometry.splice(action.payload.previousIndex, 1)[0];
            
            state.points.splice(action.payload.newIndex, 0, item);
            state.line.geometry.splice(action.payload.newIndex, 0, coordinates);

            return state;
        default:
            return state;
    }
};