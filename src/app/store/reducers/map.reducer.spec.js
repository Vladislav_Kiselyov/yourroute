import mapReducer from './map';


describe('Тест редьюсера mapReducer', () => {
    it('Корректно обрабатывает экшн ADD_NEW_WAYPOINT', () => {
        const newPoint = { title: 'Новая точка', coordinates: [0, 0] };
        const state = mapReducer({
            points: null,
            line: null,
            center: []
        }, { type: 'ADD_NEW_WAYPOINT', payload: newPoint });

        expect(state.center).toEqual([0, 0]);
        expect(state.points).toBeTruthy();
        expect(state.line).toBeTruthy();
    });

    it('Корректно обрабатывает экшн DELETE_WAYPOINT', () => {
		const initialState = {
            points: new window.ymaps.GeoObjectCollection(),
            line: new window.ymaps.Polyline(),
            center: [1, 1]
        };
        const state = mapReducer(initialState, { type: 'DELETE_WAYPOINT', payload: 0 });

        expect(state.points.splice).toHaveBeenCalled();
        expect(state.line.geometry.remove).toHaveBeenCalled();
        expect(state.center).toEqual([0, 0]);
    });

    it('Корректно обрабатывает экшн MOVE_WAYPOINT', () => {
        const initialState = {
            points: new window.ymaps.GeoObjectCollection(),
            line: new window.ymaps.Polyline(),
            center: [0, 0]
        };
        const state = mapReducer(initialState, {
            type: 'MOVE_WAYPOINT',
            payload: {
                previousIndex: 0,
                newIndex: 1
            }
        });

        expect(state.points.splice).toHaveBeenCalled();
        expect(state.line.geometry.splice).toHaveBeenCalled();
        expect(state.center).toEqual([0, 0]);
    });
});