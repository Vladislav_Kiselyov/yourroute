export default function newWaypointReducer(state = [], action) {
    switch (action.type) {
        case 'GET_WAYPOINT_SEARCH_RESULT':
            state = action.payload;
            return state;
        default:
            return state;
    }
};