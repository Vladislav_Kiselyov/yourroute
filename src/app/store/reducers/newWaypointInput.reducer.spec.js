import newWaypointReducer from './newWaypointInput';


describe('Тест редьюсера newWaypointReducer', () => {
    it('Корректно обрабатывает экшн GET_WAYPOINT_SEARCH_RESULT', () => {
        const searchResult = [{ title: 'Новая точка', coordinates: [0, 0] }];
        const state = newWaypointReducer([], { 
            type: 'GET_WAYPOINT_SEARCH_RESULT', payload: searchResult 
        });

        expect(state).toEqual(searchResult);
    });
});