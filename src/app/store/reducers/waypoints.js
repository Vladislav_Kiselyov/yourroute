export default function waypointsReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_NEW_WAYPOINT':
            state.push(action.payload)
            return state;
        case 'DELETE_WAYPOINT':
            state.splice(action.payload, 1);
            return state;
        case 'MOVE_WAYPOINT':
            let item = state.splice(action.payload.previousIndex, 1)[0];
            state.splice(action.payload.newIndex, 0, item);
            return state;
        default:
            return state;
    }
};