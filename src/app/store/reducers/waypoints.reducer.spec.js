import waypointsReducer from './waypoints';


describe('Тест редьюсера waypointsReducer', () => {
    it('Корректно обрабатывает экшн ADD_NEW_WAYPOINT', () => {
		const newPoint = { title: 'Новая точка', coordinates: [0, 0] };
        const state = waypointsReducer([], { type: 'ADD_NEW_WAYPOINT', payload: newPoint });

        expect(state).toEqual([newPoint]);
    });

    it('Корректно обрабатывает экшн DELETE_WAYPOINT', () => {
		const newPoint = { title: 'Новая точка', coordinates: [0, 0] };
        const state = waypointsReducer([newPoint], { type: 'DELETE_WAYPOINT', payload: 0 });

        expect(state).toEqual([]);
    });

    it('Корректно обрабатывает экшн MOVE_WAYPOINT', () => {
		const newPoint1 = { title: 'Новая точка 1', coordinates: [0, 0] };
		const newPoint2 = { title: 'Новая точка 2', coordinates: [1, 1] };
        const state = waypointsReducer([newPoint1, newPoint2], {
            type: 'MOVE_WAYPOINT',
            payload: {
                previousIndex: 0,
                newIndex: 1
            }
        });
        
        expect(state).toEqual([newPoint2, newPoint1]);
    });
});