import 'jest-enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { callbackify } from 'util';

configure({ adapter: new Adapter() });
global.ymaps =  {
	ready: function(callback) {
		callback();
	},
	geocode: function() {
		return new Promise((resolve, reject) => {
			resolve([0, 0]);
		});
	},
	geometry: {

	}
}

class Map {
	constructor() {}
}
class Placemark {
	constructor() {}
	events = {
		add: function() {}
	}
}
class GeoObjectCollection {
	constructor() {}
	add = function() {}
	splice = jest.fn(() => { 
		return {
			get: function() {} 
		}
	})
	getLength = jest.fn(() => 1 )
	get = function() {
		return {
			geometry: {
				getCoordinates: function() {
					return [0, 0];
				}
			}
		}
	}
}
class Polyline {
	constructor() {} 
	geometry = {
		set: function() {},
		getLength: function() {},
		remove: jest.fn(),
		splice: jest.fn(() => [])
	}
}
class LineString {
	constructor() {}
}

global.ymaps.Map = Map;
global.ymaps.Placemark = Placemark;
global.ymaps.GeoObjectCollection = GeoObjectCollection;
global.ymaps.Polyline = Polyline;
global.ymaps.geometry.LineString = LineString;